package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Calculates the number of alive neighbours
func sumOfAliveNeighbours(x, y, height, width int, world [][]byte) int {
	// Start the Sum at 0
	sum := 0;

	// Start a for loop that looks at every neighbour of the element at x,y
	for i := -1; i < 2; i++ {
		for j := -1; j < 2; j++ {
			// Stops it from adding itself to the sum
			if (i != 0 || j != 0) {
				neighbourX := x+i
				neighbourY := y+j

				// Checks the x-axis whether it's an edge and if so connects to the other side
				if neighbourX == width {
					neighbourX = 0
				} else if neighbourX == -1 {
					neighbourX = width - 1
				}

				// Checks the y-axis whether it's an edge and if so connects to the other side
				if neighbourY == height {
					neighbourY = 0
				} else if neighbourY == -1 {
					neighbourY = height - 1
				}

				if world[neighbourY][neighbourX] == 255 {
					sum++;
				}
			}
		}
	}
	return sum;
}

// Distributor divides the work between workers and interacts with other goroutines.
func distributor(p golParams, d distributorChans, alive chan []cell) {
	// Create the 2D slice to store the world.
	world := make([][]byte, p.imageHeight)
	for i := range world {
		world[i] = make([]byte, p.imageWidth)
	}

	// Request the io goroutine to read in the image with the given filename.
	d.io.command <- ioInput
	d.io.filename <- strings.Join([]string{strconv.Itoa(p.imageWidth), strconv.Itoa(p.imageHeight)}, "x")

	// The io goroutine sends the requested image byte by byte, in rows.
	for y := 0; y < p.imageHeight; y++ {
		for x := 0; x < p.imageWidth; x++ {
			val := <-d.io.inputVal
			if val != 0 {
				//fmt.Println("Alive cell at", x, y)
				world[y][x] = val
			}
		}
	}

	// Calculate the new state of Game of Life after the given number of turns.
	for turns := 0; turns < p.turns; turns++ {
		immutableWorld := make([][]byte, p.imageHeight)
		for i := range immutableWorld {
			immutableWorld[i] = make([]byte, p.imageWidth)
		}

		for y := 0; y < p.imageHeight; y++ {
			for x := 0; x < p.imageWidth; x++ {
				immutableWorld[y][x] = world[y][x]
			}
		}

		for y := 0; y < p.imageHeight; y++ {
			for x := 0; x < p.imageWidth; x++ {
				sum := sumOfAliveNeighbours(x, y, p.imageHeight, p.imageWidth, immutableWorld)
				// If current cell is alive
				if immutableWorld[y][x] == 255 {
					if sum < 2 || sum > 3 {
						world[y][x] = 0
					}
				} else { // If current cell is dead
					if sum == 3 {
						world[y][x] = 255
					}
				}
			}
		}
	}

	// Create an empty slice to store coordinates of cells that are still alive after p.turns are done.
	var finalAlive []cell
	// Go through the world and append the cells that are still alive.
	for y := 0; y < p.imageHeight; y++ {
		for x := 0; x < p.imageWidth; x++ {
			if world[y][x] != 0 {
				finalAlive = append(finalAlive, cell{x: x, y: y})
			}
		}
	}

	// Make sure that the Io has finished any output before exiting.
	d.io.command <- ioCheckIdle
	<-d.io.idle

	// output PGM file
	//go writeOutToPgm(p, d, world)

	var filenameBuilder strings.Builder
	dt := time.Now()
	filenameBuilder.WriteString(dt.Format("01-02-2006 15:04:05"))
	filenameBuilder.WriteString("_")
	filenameBuilder.WriteString(strconv.Itoa(p.imageWidth))
	filenameBuilder.WriteString("x")
	filenameBuilder.WriteString(strconv.Itoa(p.imageHeight))
	filenameBuilder.WriteString("_t:")
	filenameBuilder.WriteString(strconv.Itoa(p.turns))

	d.io.command <- ioOutput
	d.io.filename <- filenameBuilder.String()
	for y := 0; y < p.imageHeight; y++ {
        for x := 0; x < p.imageWidth; x++ {
            d.io.outputVal <- world[y][x]
        }
    }

	fmt.Println("write done")
	d.io.command <- ioCheckIdle
	<-d.io.idle

	// Return the coordinates of cells that are still alive.
	alive <- finalAlive
}

/*
func writeOutToPgm(p golParams, d distributorChans, w [][]byte){
	var filenameBuilder strings.Builder
	dt := time.Now()
	filenameBuilder.WriteString(dt.Format("01-02-2006 15:04:05"))
	filenameBuilder.WriteString("_")
	filenameBuilder.WriteString(strconv.Itoa(p.imageWidth))
	filenameBuilder.WriteString("x")
	filenameBuilder.WriteString(strconv.Itoa(p.imageHeight))
	filenameBuilder.WriteString("_t:")
	filenameBuilder.WriteString(strconv.Itoa(p.turns))

	d.io.command <- ioOutput
	d.io.filename <- filenameBuilder.String()
	for y := 0; y < p.imageHeight; y++ {
        for x := 0; x < p.imageWidth; x++ {
            d.io.outputVal <- w[y][x]
        }
    }
}
*/
